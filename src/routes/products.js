"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var routerProducts = express_1.default.Router();
var array = [];
routerProducts.get('/listar', function (req, res) {
    if (array.length)
        res.json(array);
    else
        res.json({ error: "No hay productos cargados" });
});
routerProducts.get('/listar/:id', function (req, res, next) {
    try {
        var id_1 = parseInt(req.params.id);
        if (isNaN(id_1))
            res.json({ error: "El id del produto no es un numero" });
        else {
            var index = array.findIndex(function (e) { return e.id === id_1; });
            if (index === -1)
                res.json({ error: "Producto no encontrado" });
            else
                res.json(array[index]);
        }
    }
    catch (err) {
        next(err);
    }
});
routerProducts.post('/guardar', function (req, res) {
    var _a;
    var _b = req.body, title = _b.title, price = _b.price, thumbnail = _b.thumbnail;
    if (!(title && price && thumbnail)) {
        res.json({ error: "Hay parametros vacios o indefinidos" });
    }
    else {
        var newProduct = { id: (((_a = array[array.length - 1]) === null || _a === void 0 ? void 0 : _a.id) || 0) + 1, title: title, price: price, thumbnail: thumbnail };
        array.push(newProduct);
        res.json(newProduct);
    }
});
routerProducts.put('/actualizar/:id', function (req, res) {
    var _a = req.body, title = _a.title, price = _a.price, thumbnail = _a.thumbnail;
    var id = parseInt(req.params.id);
    if (isNaN(id))
        res.json({ error: "El id del produto no es un numero" });
    else {
        var index = array.findIndex(function (e) { return e.id === id; });
        if (index === -1) {
            res.json({ error: "No existe un producto con el id" + id });
        }
        else {
            if (!(title && price && thumbnail)) {
                res.json({ error: "Hay parametros vacios o indefinidos" });
            }
            else {
                var newProduct = { id: id, title: title, price: price, thumbnail: thumbnail };
                array[index] = newProduct;
                res.json(newProduct);
            }
        }
    }
});
routerProducts.delete('borrar/:id', function (req, res) {
    var id = parseInt(req.params.id);
    if (isNaN(id))
        res.json({ error: "El id del produto no es un numero" });
    else {
        var index = array.findIndex(function (e) { return e.id === id; });
        if (index === -1) {
            res.json({ error: "No existe un producto con el id " + id });
        }
        else {
            var aux = array[index];
            array.splice(index, 1);
            res.json(aux);
        }
    }
});
exports.default = routerProducts;
