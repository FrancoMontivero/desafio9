//@ts-nocheck
import app from './src/app';

app.listen(app.get('port'), () => {
    console.log(`listen in the port ${app.get('port')}`);
})